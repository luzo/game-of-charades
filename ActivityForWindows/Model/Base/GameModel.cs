﻿using ActivityForWindows.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.Model
{
    class GameModel
    {
        private Queue<TeamModel> teams;
        private ActivityModel actualActivity;
        private Dictionary<ACTIVITY_TYPE, List<ActivityModel>> activities;

        public GameModel()
        {
            this.teams = new Queue<TeamModel>();
            var culture = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            
            Task.Run(async () =>
            {
                string path = "XML/activities_"+ culture + ".xml";
                string pathCustom = "XML/activities_custom_" + culture + ".xml";

                if (!File.Exists(path))
                {
                    path = "XML/activities_en.xml";
                }
                if (!File.Exists(pathCustom))
                {
                    pathCustom = "XML/activities_custom_en.xml";
                }

                this.activities = await ActivitiesXMLParser.ParseXML(path,pathCustom);
            });
        }

        public void AddPlayers(Queue<PlayerModel> players, int teamName)
        {
            TeamModel team = new TeamModel(teamName);
            team.AddPlayers(players);
            teams.Enqueue(team);
        }

        public Queue<TeamModel> GetTeams()
        {
            return teams;
        }

        public ActivityModel GetActualActivity()
        {
            return this.actualActivity;
        }

        public PlayerModel GetActualPlayer()
        {
            TeamModel actualTeam = teams.Peek();
            PlayerModel actualPlayer = actualTeam.GetActualPlayer();
            return actualPlayer;
        }

        public PlayerModel GetNextPlayer()
        {
            TeamModel actualTeam = teams.Dequeue();
            PlayerModel actualPlayer = actualTeam.GetNextPlayer();
            teams.Enqueue(actualTeam);
            return actualPlayer;
        }

        public TeamModel GetActualTeam()
        {
            return teams.Peek();
        }

        public ActivityModel GetActivityModel(int index, ACTIVITY_TYPE type)
        {         
           return activities[type][index];
        }

        public int GetActivitiesCount(ACTIVITY_TYPE type)
        {
            return activities[type].Count();
        }

        public void setActivityModel(ActivityModel actualActivity)
        {
            this.actualActivity = actualActivity;
        }

        public ActivityModel getActivityModel()
        {
            return this.actualActivity;
        }
    }

}
