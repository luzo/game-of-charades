﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.Model
{
    class PlayerModel
    {
        private string playerName;
        private int teamNumber;

        public string GetPlayerName()
        {
            return playerName;
        }

        public int GetTeamNumber()
        {
            return teamNumber;
        }

        public PlayerModel(string name, int team)
        {
            this.playerName = name;
            this.teamNumber = team;
        }
    }
}
