﻿using ActivityForWindows.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.Model
{
    class ActivityModel
    {
        private string description;
        private int numberOfPoints;
        private ACTIVITY_TYPE type;

        public ActivityModel(ACTIVITY_TYPE type, string description, int points)
        {
            this.description = description;
            this.numberOfPoints = points;
            this.type = type;
        }

        public string GetDescription()
        {
            return this.description;
        }

        public int GetPoints() {
            return this.numberOfPoints;
        }

        public ACTIVITY_TYPE GetActivityType()
        {
            return this.type;
        }
    }
}
