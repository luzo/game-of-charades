﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.Model
{
    class TeamModel
    {
        private Queue<PlayerModel> players;
        private int teamNumber;
        private int points;

        public TeamModel(int number) {
            this.players = new Queue<PlayerModel>();
            this.teamNumber = number;
            this.points = 0;
        }

        public void AddPlayer(PlayerModel player)
        {
            this.players.Enqueue(player);
        }

        public Queue<PlayerModel> getPlayers()
        {
            return players;
        }

        public void AddPlayers(Queue<PlayerModel> players)
        {
            this.players = players;
        }

        public int GetTeamNumber() {
            return teamNumber;
        }

        public PlayerModel GetActualPlayer()
        {
            PlayerModel actualPlayer = players.Peek();
            return actualPlayer;
        }

        public PlayerModel GetNextPlayer() {
            PlayerModel actualPlayer = players.Dequeue();
            players.Enqueue(actualPlayer);
            return actualPlayer;
        }

        public int GetPoints()
        {
            return this.points;
        }

        public void AddPoints(int points) {

            this.points += points;
        }
    }
}
