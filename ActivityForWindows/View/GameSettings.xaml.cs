﻿using System.Reactive.Linq;
using System.Linq;
using Reactive.Bindings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ActivityForWindows.ViewModel;
using System.Diagnostics;
using System.Reactive.Subjects;
using System;
using ActivityForWindows.Model;
using ActivityForWindows.Helpers;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using System.Collections.Generic;
using Windows.UI.Popups;
using Windows.ApplicationModel.Resources;

namespace ActivityForWindows.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GameSettings : Page
    {
        GameSettingsViewModel viewModel;

        public GameSettings()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.viewModel = new GameSettingsViewModel();

            this.playerNameField.Visibility = Visibility.Collapsed;
            this.addPlayerButton.Visibility = Visibility.Collapsed;
            this.teamPicker.Visibility = Visibility.Collapsed;
            this.textBlock.Visibility = Visibility.Collapsed;
            this.startGameButton.Visibility = Visibility.Collapsed;
            this.addPlayerButton.IsEnabled = false;
            this.playerNameField.IsEnabled = false;

            this.Focus(FocusState.Keyboard);

            var inputSignal = this.viewModel.inputText.DistinctUntilChanged().Select(name => !(name.Count() < 2));
            var selectedTeamsSignal = this.viewModel.selectedNumberOfTeams.Select(team => team > 1);

            inputSignal.CombineLatest(selectedTeamsSignal, (triggerable, selectedNumber) => triggerable && selectedNumber).Subscribe(executable =>
            {
                this.viewModel.executableAdd = executable;
                this.addPlayerButton.IsEnabled = executable;
            });

            var players = this.viewModel.playerAdded.Scan(0, (sum, player) =>
            {
                this.playerNameField.Text = "";
                this.playerNamesLabel.Text += (sum + 1) + ". " + player.GetPlayerName() + " - team " + player.GetTeamNumber() + System.Environment.NewLine;
                return sum + 1;
            }
            );

            var teamsSignal = this.viewModel.selectedNumberOfTeams;
            players.CombineLatest(teamsSignal, (sum, numberOfTeams) => sum >= numberOfTeams * 2).DistinctUntilChanged().Subscribe(triggerable =>
            {
                this.viewModel.executableStart = triggerable;
                this.startGameButton.Visibility = triggerable ? Visibility.Visible : Visibility.Collapsed;
            });

         /*  
          if we do not want to erase players

            if (e.Parameter.GetType().Equals(typeof(Queue<TeamModel>)))
            {
                var teams = (Queue<TeamModel>)e.Parameter;
                foreach (var team in teams)
                {
                    foreach (var player in team.getPlayers())
                    {
                        this.viewModel.playerAdded.OnNext(player);
                    }
                }
            }
          */
        }

        private void PlayerNameField_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.viewModel.inputText.OnNext(this.playerNameField.Text);
        }

        public async void StartGame(object sender, RoutedEventArgs e)
        {
            ResourceLoader resourceLoader = new ResourceLoader();

            if (this.viewModel.executableStart)
            {
                this.viewModel.playerAdded.OnCompleted();

                if (this.viewModel.GameStartAllowed(this.viewModel.numberOfTeams))
                {
                    TurnPageViewModel deferedVM = new TurnPageViewModel();
                    this.viewModel.deferViewModel(deferedVM as BaseViewModel);
                    this.Frame.Navigate(typeof(View.TurnPage), deferedVM);
                }
                else
                {
                    var dialog = new MessageDialog(resourceLoader.GetString("gameSettings_alert_players_text"));
                    dialog.Commands.Add(new UICommand("OK"));
                    var result = await dialog.ShowAsync();
                   
                    this.Frame.Navigate(typeof(View.GameSettings), this.viewModel.GetTeams());
                }

            }
        }

        public void SetNumberOfTeams(object sender, RoutedEventArgs e)
        {
            int number = 0;
            try
            {
                if ((number = Int32.Parse(this.teamNumberField.Text)) > 1)
                {
                    this.viewModel.selectedNumberOfTeams.OnNext(number);

                    for (int i = 1; i <= number; i++)
                    {
                        this.teamPicker.Items.Add(i);
                    }
                    this.teamNumberField.Visibility = Visibility.Collapsed;
                    this.teamNumberButton.Visibility = Visibility.Collapsed;
                    this.playerNameField.Visibility = Visibility.Visible;
                    this.addPlayerButton.Visibility = Visibility.Visible;
                    this.teamPicker.Visibility = Visibility.Visible;
                    this.textBlock.Visibility = Visibility.Visible;

                    this.teamPicker.SelectedIndex = 0;

                    this.playerNameField.IsEnabled = true;

                    this.playerNameField.Focus(FocusState.Keyboard);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            };

        }

        public void AddNewPlayer(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.executableAdd)
            {
                string name = playerNameField.Text;
                int team = (int)teamPicker.SelectedValue;

                this.viewModel.AddPlayer(name, team);
                this.playerNameField.Focus(FocusState.Keyboard);
            }

        }

    }
}