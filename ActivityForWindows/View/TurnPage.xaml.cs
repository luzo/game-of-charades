﻿using System.Reactive.Linq;
using System.Linq;
using Reactive.Bindings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ActivityForWindows.ViewModel;
using System.Diagnostics;
using System.Reactive.Subjects;
using System;
using ActivityForWindows.Model;
using Windows.UI.Xaml.Navigation;
using System.Collections.Generic;
using ActivityForWindows.Helpers;
using ActivityForWindows.Extensions;
using Windows.UI.Xaml.Media.Imaging;
using Windows.ApplicationModel.Resources;

namespace ActivityForWindows.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

    public sealed partial class TurnPage : Page
    {
        TurnPageViewModel viewModel;
        public TurnPage()
        {
            this.InitializeComponent();
        }

        private void NextTurn() {
            this.viewModel.GetNextPlayer();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            ResourceLoader resourceLoader = new ResourceLoader();
            
            Debug.WriteLine("navigated to Turn Page");

            if (e.Parameter.GetType().Equals(typeof(TurnPageViewModel)))
            {
                this.viewModel = (TurnPageViewModel)e.Parameter;
            }
            else {
                throw new System.ArgumentException("No players in model");
            }

            this.viewModel.actualPlayerSignal.Subscribe(player =>
            {

                this.playerNameText.Text = player.GetPlayerName();
            
                this.teamText.Text = resourceLoader.GetString("turnPage_team_text") + " " + player.GetTeamNumber();
                this.teamPointsText.Text = this.viewModel.GetPointsForActualTeam() + " " + resourceLoader.GetString("turnPage_points_text");
            });

            this.viewModel.GetActualPlayer();
            this.setActivities();
        }

        private void setActivities() {

            ResourceLoader resourceLoader = new ResourceLoader();

            this.viewModel.activitiesSignal.Subscribe(activityList => {

                for (int index = 0; index < activityList.Count(); index++)
                {
                    var activity = activityList.ElementAt(index);
                    string type = activity.GetActivityType().ToString().Replace("activity","");
                    string points = activity.GetPoints().ToString();
                    string name = type;

                    TextBlock pointsBlock = (TextBlock)this.FindName("points_" + (index + 1));
                    TextBlock nameBlock = (TextBlock)this.FindName("name_" + (index + 1));
                    Image image = (Image)this.FindName("image_" + (index + 1));

                    var imageSource = new BitmapImage(new Uri("ms-appx:///Assets/Icons/"+ type.ToLower() + "_icon.png"));
                    image.Source = imageSource;

                    pointsBlock.Text = points;
                    nameBlock.Text = resourceLoader.GetString("activity_" + type.ToLower());
                }

            });
            this.viewModel.GetNextActivities();
        }

        private void actionButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            string name = button.Name.ToString();
            
            int index = name.Split('_').Last().ToInt();
            ActivityPageViewModel newVM = new ActivityPageViewModel();
   
            this.viewModel.deferViewModel(newVM,index);
            this.Frame.Navigate(typeof(View.ActivityPage), newVM);
        }
    }

}
