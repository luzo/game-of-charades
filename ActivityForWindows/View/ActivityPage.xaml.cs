﻿using System.Reactive.Linq;
using System.Linq;
using Reactive.Bindings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ActivityForWindows.ViewModel;
using System.Diagnostics;
using System.Reactive.Subjects;
using System;
using ActivityForWindows.Model;
using Windows.UI.Xaml.Navigation;
using ActivityForWindows.Extensions;
using System.Threading;
using System.IO;
using Windows.Storage;
using Windows.ApplicationModel.Resources;

namespace ActivityForWindows.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ActivityPage : Page
    {
        ActivityPageViewModel viewModel;


        public ActivityPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            ResourceLoader resourceLoader = new ResourceLoader();
            Debug.WriteLine("navigated to Activity Page");

            this.timerText.Text = "60";

            if (e.Parameter.GetType().Equals(typeof(ActivityPageViewModel)))
            {
                this.viewModel = (ActivityPageViewModel)e.Parameter;
            }
            else
            {
                throw new System.ArgumentException("No players in model");
            }

            this.viewModel.activityModelSignal.Subscribe(activity =>
            {
                this.activityDescription.Text = activity.GetDescription();
                this.activityPoints.Text = activity.GetPoints().ToString();
                this.activityTypeText.Text = activity.GetActivityType().ToString();
            });

            this.viewModel.actualPlayerSignal.Subscribe(player =>
            {
                this.playerNameText.Text = player.GetPlayerName() + " " + resourceLoader.GetString("turnPage_team_text") + " " + player.GetTeamNumber();
            });

            StorageFolder Folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            Folder = await Folder.GetFolderAsync("Assets");
            Folder = await Folder.GetFolderAsync("Sounds");

            this.viewModel.tickSound = await Folder.GetFileAsync("tick.mp3");
            this.viewModel.gongSound = await Folder.GetFileAsync("gong.mp3");

            this.media.SetSource(await this.viewModel.tickSound.OpenAsync(FileAccessMode.Read), this.viewModel.tickSound.ContentType);
            this.media.AutoPlay = true;
            
            this.viewModel.loadedSignal.OnNext(true);

            this.viewModel.timer.Tick += OnTimerTick;
            this.viewModel.timer.Interval = new TimeSpan(0, 0, 1);
            this.viewModel.timer.Start();
        }

        public async void OnTimerTick(object sender, object e)
        {
            int currentTime = Int32.Parse(this.timerText.Text) - 1;
            this.timerText.Text = currentTime.ToString();
            this.media.Play();
            if (currentTime <= 0)
            {
                this.media.SetSource(await this.viewModel.gongSound.OpenAsync(FileAccessMode.Read), this.viewModel.gongSound.ContentType);
                this.media.Play();

                ((DispatcherTimer)sender).Stop();
            }
        }

        public void EndActivity(object sender, RoutedEventArgs e)
        {
            this.viewModel.timer.Stop();
            Button button = (Button)sender;
            string name = button.Name.ToString();

            int index = name.Split('_').Last().ToInt();

            if (index == 1)
            {
                this.viewModel.addPointsSignal.OnNext(this.viewModel.GetActualActivity().GetPoints());
            }

            TurnPageViewModel newVM = new TurnPageViewModel();

            this.viewModel.deferViewModel(newVM, index);
            this.Frame.Navigate(typeof(View.TurnPage), newVM);
        }
    }
}