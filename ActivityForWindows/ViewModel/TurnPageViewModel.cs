﻿using ActivityForWindows.Extensions;
using ActivityForWindows.Helpers;
using ActivityForWindows.Model;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;

namespace ActivityForWindows.ViewModel
{
    class TurnPageViewModel:BaseViewModel
    {
        public ISubject<List<ActivityModel>> activitiesSignal;
        public ISubject<PlayerModel> actualPlayerSignal;
        private List<ActivityModel> activitiesList;
        private PlayerModel actualPlayer;

        public TurnPageViewModel()
        {
            this.activitiesSignal = new Subject<List<ActivityModel>>();
            this.actualPlayerSignal = new Subject<PlayerModel>();
            this.activitiesList = new List<ActivityModel>();
            activitiesSignal.Subscribe(activities => this.activitiesList = activities);
            actualPlayerSignal.Subscribe(player => this.actualPlayer = player);

        }

        public TurnPageViewModel(BaseViewModel baseViewModel)
        {
            this.activitiesSignal = new Subject<List<ActivityModel>>();
            this.actualPlayerSignal = new Subject<PlayerModel>();
            this.activitiesList = new List<ActivityModel>();
        }

        public void GetNextPlayer()
        {
            PlayerModel nextPlayer = this.gameModel.GetNextPlayer();
            this.actualPlayerSignal.OnNext(nextPlayer);
        }

        public int GetPointsForActualTeam()
        {
            return this.gameModel.GetActualTeam().GetPoints();
        }

        public void GetActualPlayer()
        {
            PlayerModel nextPlayer = this.gameModel.GetActualPlayer();
            this.actualPlayerSignal.OnNext(nextPlayer);
        }

        public void GetNextActivities()
        {
            List<ActivityModel> activities = new List<ActivityModel>();
            int numberOfTypes = Enum.GetValues(typeof(ACTIVITY_TYPE)).Length;

            for (int number = 0; number < numberOfTypes; number++)
            {
                ACTIVITY_TYPE type = (ACTIVITY_TYPE)(numberOfTypes.GetRandomForMaximum());
                if (Enum.IsDefined(typeof(ACTIVITY_TYPE), type)) {
                    int index = this.gameModel.GetActivitiesCount(type).GetRandomForMaximum();
                    ActivityModel activity = this.gameModel.GetActivityModel(index, type);
                    activities.Add(activity);
                } else
                {
                    number--;
                } 
            }

            this.activitiesSignal.OnNext(activities);
        }
        
        public ActivityPageViewModel deferViewModel(ActivityPageViewModel newVM, int index)
        {
            this.gameModel.setActivityModel(activitiesList[index-1]);
            newVM = (ActivityPageViewModel)base.deferViewModel(newVM);
       
            return newVM;
        }
    }
}
