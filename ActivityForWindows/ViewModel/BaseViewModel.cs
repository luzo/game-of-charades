﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive;
using ActivityForWindows.Model;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Diagnostics;
using ActivityForWindows.Helpers;
using ActivityForWindows.Extensions;

namespace ActivityForWindows.ViewModel
{
    class BaseViewModel
    {
        public GameModel gameModel;

        public ISubject<Exception> exception;

        public BaseViewModel()
        {
            this.gameModel = new GameModel();
            this.exception = new Subject<Exception>();
        }

        protected GameModel getModel() {
            return gameModel;
        }

        public virtual BaseViewModel deferViewModel(BaseViewModel newVM)
        {
            newVM.gameModel = this.gameModel;
            return newVM;
        }
    }
}
