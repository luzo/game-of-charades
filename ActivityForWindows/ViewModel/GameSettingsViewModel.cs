﻿using ActivityForWindows.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.ViewModel
{
    class GameSettingsViewModel:BaseViewModel
    {
        public ISubject<PlayerModel> playerAdded;
        public ISubject<string> inputText;
        public ISubject<int> selectedNumberOfTeams;

        public bool executableAdd;
        public bool executableStart;
        public int numberOfTeams;

        public GameSettingsViewModel()
        {
            this.playerAdded = new Subject<PlayerModel>();
            this.inputText = new Subject<string>();
            this.selectedNumberOfTeams = new Subject<int>();
            this.executableAdd = false;
            this.executableStart = false;

            selectedNumberOfTeams.Subscribe(numberOfTeams => this.numberOfTeams = numberOfTeams);

            playerAdded.Aggregate(new Queue<PlayerModel>(), (sum, player) =>
            {
                sum.Enqueue(player);
                return sum;
            }).Subscribe(players =>
            {
                 var groupedPlayers = from player in players
                                      group player by player.GetTeamNumber();

                 foreach (var group in groupedPlayers)
                 {
                     gameModel.AddPlayers(new Queue<PlayerModel>(group.ToList()), group.Key);
                 }

             });
        }

        public bool GameStartAllowed(int teamsNumber)
        {
            var teams = gameModel.GetTeams();

            if (teams.Count != teamsNumber) return false;

            foreach (var team in teams)
            {
                if (team.getPlayers().Count < 2) return false;
            }

            return true;
        }

        public bool AddPlayer(string name, int team)
        {
            try
            {
                PlayerModel playerModel = new PlayerModel(name, team);
                this.playerAdded.OnNext(playerModel);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public Queue<TeamModel> GetTeams()
        {
            return gameModel.GetTeams();
        }
    }
}
