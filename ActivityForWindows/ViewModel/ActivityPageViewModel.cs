﻿using ActivityForWindows.Extensions;
using ActivityForWindows.Helpers;
using ActivityForWindows.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;

namespace ActivityForWindows.ViewModel
{
    class ActivityPageViewModel: BaseViewModel
    {
        public DispatcherTimer timer;
        public StorageFile tickSound;
        public StorageFile gongSound;
        public ISubject<ActivityModel> activityModelSignal;
        public ISubject<PlayerModel> actualPlayerSignal;
        public ISubject<int> addPointsSignal;
        public ISubject<bool> loadedSignal;
        private ActivityModel actualActivity;

        public ActivityPageViewModel()
        {
            this.timer = new DispatcherTimer();
            this.activityModelSignal = new Subject<ActivityModel>();
            this.actualPlayerSignal = new Subject<PlayerModel>();
            this.addPointsSignal = new Subject<int>();
            this.loadedSignal = new Subject<bool>();

            loadedSignal.Subscribe(next => this.SetUI());

            this.activityModelSignal.Subscribe(
                activity => actualActivity = activity
            );

            this.addPointsSignal.Subscribe(
                points => this.gameModel.GetActualTeam().AddPoints(points)     
            );
        }

        public ActivityModel GetActualActivity()
        {
            return this.actualActivity;
        }

        public void SetUI()
        {
            PlayerModel nextPlayer = this.gameModel.GetActualPlayer();
            this.actualPlayerSignal.OnNext(nextPlayer);

            ActivityModel actualActivity = this.gameModel.GetActualActivity();
            this.activityModelSignal.OnNext(actualActivity);
        }

        public TurnPageViewModel deferViewModel(TurnPageViewModel newVM, int index)
        {
            newVM = (TurnPageViewModel)base.deferViewModel(newVM);
            newVM.GetNextPlayer();

            return newVM;
        }
    }
}
