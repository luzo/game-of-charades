﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.Extensions
{
    public static class StringExtension
    {
        public static int ToInt(this string numberText)
        {
            int number = 0;
            Int32.TryParse(numberText, out number);
            return number;
        }

    }
}
