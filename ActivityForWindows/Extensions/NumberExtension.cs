﻿using ActivityForWindows.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Troschuetz.Random.Generators;

namespace ActivityForWindows.Extensions
{
    public static class NumberExtension
    {
        private static NR3Q1Generator randomGenerator = new NR3Q1Generator();
        public static int GetRandomForMaximum(this int maximum)
        {
            int next = randomGenerator.Next() % randomGenerator.Next();
            return next % maximum;           
        }
    }
}
