﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityForWindows.Helpers
{
    public enum ACTIVITY_TYPE {
        activitySpeaking = 1,
        activityDrawing = 2,
        activityMime = 3}
}
