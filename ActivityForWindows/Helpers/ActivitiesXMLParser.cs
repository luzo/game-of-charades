﻿using ActivityForWindows.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActivityForWindows.Helpers
{
    class ActivitiesXMLParser
    {
        /* NEED TO BE REWORKED - ITS MESS */
        public static async Task<Dictionary<ACTIVITY_TYPE, List<ActivityModel>>> ParseXML(string path, string pathCustom)
        {
            XDocument doc = XDocument.Load(path);
            
            XDocument docCustom = XDocument.Load(pathCustom);

            Dictionary<ACTIVITY_TYPE, List<ActivityModel>> dictionary = new Dictionary<ACTIVITY_TYPE, List<ActivityModel>>();

            foreach (var activityType in Enum.GetValues(typeof(ACTIVITY_TYPE)))
            {
                ACTIVITY_TYPE type;
                if (Enum.TryParse(activityType.ToString(), out type))
                {
                    var descendants = doc.Descendants(activityType.ToString()).Elements("activity");
                    var descendantsCustom = docCustom.Descendants(activityType.ToString()).Elements("activity");

                    var activityList = descendants.Concat(descendantsCustom).SelectMany(activity =>
                    {
                        List<ActivityModel> list = new List<ActivityModel>();

                        string description = (string)activity.Element("description");
                        int points;

                        if (!Int32.TryParse((string)activity.Element("points"), out points))
                        {
                            points = 1;
                        };

                        ActivityModel activityModel = new ActivityModel(type, description, points);
                        list.Add(activityModel);
                        return list;
                    });

                    dictionary.Add(type, activityList.ToList());
                }
            }

            return dictionary;
        }
    }
}
